//
//  DetailViewController.swift
//  NibXib
//
//  Created by Apostol, Alexandru on 02/08/2019.
//  Copyright © 2019 Apostol, Alexandru. All rights reserved.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {

    var textFromFirstXib: String
    
    @IBOutlet weak var webView: WKWebView!
  
    override func viewDidLoad() {
        super.viewDidLoad()

        let request = URLRequest(url: URL(string: "https://google.com")!)
        
        self.webView?.load(request)
       
    
        // Do any additional setup after loading the view.
    }
    
    init(_ text: String){
       textFromFirstXib = text
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
