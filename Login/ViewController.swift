//
//  ViewController.swift
//  Login
//
//  Created by Alba Domingo, David on 29/07/2019.
//  Copyright © 2019 Alba Domingo, David. All rights reserved.
//

import UIKit
import CoreLogin

class ViewController: UIViewController {
    
    @IBOutlet weak var nameTextF: UITextField!
    @IBOutlet weak var passTextF: UITextField!
    var user: Int = 20
    
    // var passTextF = UITextField()
    // var nameTextF = UITextField()
    override func viewDidLoad() {
        super.viewDidLoad()
        // setupView()
        print("changes alex")
    }
    
    func setupView(){
        // view.addSubview(nameTextF)
        // view.addSubview(passTextF)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        guard
            let userName = nameTextF.text,
            let passWord = passTextF.text else {
            return
        }
        
        let conexion = ApiDataSource()
        conexion.getUsers(name: userName, pass: passWord) { success in
            if success {
                print("Abrir nueva ventana")
                self.performSegue(withIdentifier: "showDetail", sender: passWord)
            } else {
                print("Error de login")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail",
            let destination = segue.destination as? DetailViewController {
            // La variable data es la que se encuentra en la clase DetailViewController
            destination.data = sender as? String
        }
    }
    
    
    
    
}

