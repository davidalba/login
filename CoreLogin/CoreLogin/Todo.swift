//
//  Todo.swift
//  CoreLogin
//
//  Created by Apostol, Alexandru on 01/08/2019.
//  Copyright © 2019 Apostol, Alexandru. All rights reserved.
//

import Foundation
public struct Todos : Decodable {
   public let userId: Int
   public let id: Int
   public let title: String
   public let completed: Bool
    
   public var color: UIColor {
        return completed ? .red : .black
    }
}
