//
//  DataSourceFromApi.swift
//  CoreLogin
//
//  Created by Apostol, Alexandru on 01/08/2019.
//  Copyright © 2019 Apostol, Alexandru. All rights reserved.
//

import Foundation


open class ApiDataSource {
    
    public init(){
        
    }
    
   open func getUsers(name: String, pass: String, callback: @escaping (Bool) -> Void) {
        
        if name.isEmpty && pass.isEmpty {
            print("The values are empty")
            return
        }
        
        let passInt = Int(pass) ?? 0
        
        let url = URL(string: "https://jsonplaceholder.typicode.com/users")!
        URLSession.shared.get(url, type: [User].self) { users in
            guard let users = users else {
                callback(false)
                return
            }
            
            
            if users.first(where: { $0.username == name && $0.id == passInt }) != nil {
                callback(true)
            } else {
                print("Login not okey")
                callback(false)
            }
        }
    }
    
    open func todosURL(pass: String) -> URL {
        var urlComponent = URLComponents()
        urlComponent.scheme = "https"
        urlComponent.host = "jsonplaceholder.typicode.com"
        urlComponent.path = "/todos"
        
        urlComponent.queryItems = [
            URLQueryItem(name: "userId", value: pass)
        ]
        
        return urlComponent.url!
    }
    
    
    
}
