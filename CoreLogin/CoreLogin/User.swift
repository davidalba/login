//
//  User.swift
//  CoreLogin
//
//  Created by Apostol, Alexandru on 01/08/2019.
//  Copyright © 2019 Apostol, Alexandru. All rights reserved.
//

import Foundation



public struct User : Decodable{
    public  let id : Int
   public   let username : String
}
